import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import RetroItemsGrid from './components/retroItemsGrid';

let listOfRetroItems =  [{
      "project" : "CUIC",
      "team":  "Finnesse",
      "sprint": "195",
      "retroItem": "Adhoc changes for UX",
      "actionItem": "review mockups",
      "owner": "Venkatesh",
      "status": "open"
      },

      {

      "project" : "CUIC",
      "team":  "SM",
      "sprint": "196",
      "retroItem": "Adhoc changes for UX",
      "actionItem": "review mockups",
      "owner": "Nik",
      "status": "open"
      },

      {

      "project" : "CUIC",
      "team":  "CCX",
      "sprint": "197",
      "retroItem": "Adhoc changes for UX",
      "actionItem": "review mockups",
      "owner": "Mohak",
      "status": "open"
      } ];


class App extends Component {
  render() {
    return (
      <div className="App">
        <h1>Retro Tracker</h1>


        <RetroItemsGrid  items={listOfRetroItems} />
      </div>
    );
  }
}

export default App;