import React, {Component} from 'react';

import AddEditRetro from '../addRetro/addRetro.js';

import './style.css';

class RetroItemsGrid extends Component {

	constructor() {
		super();
		this.state = {};

		this.editRetro = this.editRetro.bind(this)
	}

	editRetro (retroItem) {
		this.setState({
			showEditRetro: true,
			activeRetroItem: retroItem
		})
	}
	
	render() {
		return (
			<div>	
				<h3> Retro List </h3>
				<div className='table-header' > 
				</div>	

				<table>
					<thead>
						<tr>
							<th>Project</th>
							<th>Team</th>
							<th>Sprint</th>
							<th>Retro Item</th>
							<th>AI</th>
							<th>Owner</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody>
						{this.props.items.length > 0 ? this.props.items.map((retroItem, index) => {
							return (
								<tr key={index} onClick={()=>{
									this.editRetro(retroItem);
								}}>
									<td>{retroItem.project}</td>
									<td>{retroItem.team}</td>
									<td>{retroItem.sprint}</td>
									<td>{retroItem.retroItem}</td>
									<td>{retroItem.actionItem}</td>
									<td>{retroItem.owner}</td>
									<td>{retroItem.status}</td>
								</tr>
							)
						}) : <div className='noitems'>"No items yet"</div>}			
					</tbody>
				</table>	

				{
					this.state.showEditRetro ? <AddEditRetro {...this.state.activeRetroItem}/> : ''
				}

			</div>
		)
	}
}

export default RetroItemsGrid;