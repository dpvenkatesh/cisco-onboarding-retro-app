import React, { Component } from 'react';
import './addRetro.css';


class AddRetro extends Component {

    constructor(props){
        super();
        this.state = Object.assign({
            project : 'CUIC',
            team : 'Anything',
            status : 'Open',
            sprint : '196',
            id : '',
            retroItem : '',
            eta: new Date(),
            owner : '',
            actionItem : ''
        }, props);
        this.onSaveClick = this.onSaveClick.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }


    componentWillReceiveProps(nextProps) {
        console.log(nextProps);
        this.setState(nextProps);
    }

    onSaveClick (){
        console.log(this.state);
    }
    
    handleChange = (e) => {
    let newState = {};

    newState[e.target.name] = e.target.value;

    this.setState(newState);
    };

    haddleDropdown1 = (e) => {
        this.setState({project:e.target.value});
    };

    haddleDropdown2 = (e) => {
       this.setState({team:e.target.value});
    };

    haddleDropdown3 = (e) => {
        this.setState({sprint:e.target.value});
    };

    haddleDropdown4 = (e) => {
        this.setState({status:e.target.value});
    };
  render() {

    return (
      <div className="marginTop details">
        <h3>Retro Details</h3>
          <div>
       <label>
        Id:
        <input type="text" name="id" value = {this.state.id} onChange={this.handleChange}/>
        </label>
        </div>
        <div>
        <label>
        Project
        <select value={this.state.project} onChange={this.haddleDropdown1}>
            <option value="CUIC">CUIC</option>
            <option value="Finesse">Finesse</option>
            <option value="UCCX">UCCX</option>
        </select>
        </label>
        <label>
            Team Name:
        <select value={this.state.team} onChange={this.haddleDropdown2}>
            <option value="Anything">Anything</option>
            <option value="Something">Something</option>
        </select>
        </label>
        </div>
        <div>
         <label>
            Sprint Name:
        <select value={this.state.sprint} onChange={this.haddleDropdown3}>
            <option value="196">196</option>
            <option value="197">197</option>
            <option value="198">198</option>
        </select>
        </label>
        </div>
        <div>
         <label>
        Retro item:
        <input type="text" name="retroItem" value = {this.state.retroItem} onChange={this.handleChange}/>
        </label>
        </div>
        <div>
         <label>
        Action item:
        <input type="text" name="actionItem" value = {this.state.actionItem} onChange={this.handleChange}/>
        </label>
        </div>
        <div>
         <label>
        Owner:
        <input type="text" name="owner" value={this.state.owner} onChange={this.handleChange}/>
        </label>
        </div>
        <div>
          <label>
        ETA:
        <input type="date" name="eta" value = {this.state.date} onChange={this.handleChange}/>
        </label>
         <label>
        Status:
        <select value={this.state.status} onChange={this.haddleDropdown4}>
            <option value="Open">Open</option>
            <option value="Closed">Closed</option>
        </select>
         </label>
         </div>
         <div>
         <input type="button" onClick={this.onSaveClick} value="Save" /> 
         </div>
      </div>
    );
  }
}

export default AddRetro;
