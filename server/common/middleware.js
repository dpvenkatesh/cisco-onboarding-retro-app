var bodyParser = require('body-parser'),
	cors = require('cors');

module.exports.middleware = function(app,express) {
	app.use(cors());
	app.use(bodyParser.json({limit: '5mb'}));
	app.use(bodyParser.urlencoded({extended: true,limit: '5mb'}));
	app.use(express.static(__dirname + '/../../public'));
	app.use('/', function (req, res, next) {
		console.log("req in route");
	  next();
	});
	app.use('/user_mgmt/', function (req, res, next) {
		console.log("inside user_mgmt");
		next();
	});
};