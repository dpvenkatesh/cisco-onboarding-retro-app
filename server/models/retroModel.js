const async  = require('async');
const crypto=require("crypto");
const MongoClient = require('mongodb').MongoClient
const conf = require('../conf/config');

module.exports = {
    getReport:function (res) {
        MongoClient.connect(conf.development.MONGODB_URI, function(err, db) {
            var collection = db.collection('retro');
            collection.find({}).toArray(function(err, docs) {
                res.send(docs);
            });

        });
    },
    saveRetroItems:function (itm,res) {
        MongoClient.connect(conf.development.MONGODB_URI, function(err, db) {
            var collection = db.collection('retro');
            collection.insertOne(itm).toArray(function(err, docs) {
                res.send(docs);
            });
        });
    },
    getAllReport:function (res) {
        MongoClient.connect(conf.development.MONGODB_URI, function(err, db) {
            var collection = db.collection('retro');
            collection.find({}).toArray(function(err, docs) {
                res.send(docs);
            });
        });
    },
    getFilterReport:function (field,vl,res) {
        MongoClient.connect(conf.development.MONGODB_URI, function(err, db) {
            var collection = db.collection('retro');
            let itm ={};
            itm[field]=vl;
            collection.find(itm).toArray(function(err, docs) {
                res.send(docs);
            });
        });
    },
    getFilters:function (res) {
        MongoClient.connect(conf.development.MONGODB_URI, function(err, db) {
            if(err) throw err;

            let projectsCollection = db.collection('project_filter');
            let statusCollection = db.collection('status_filter');
            let sprintCollection = db.collection('sprint_filter');
            let teamCollection = db.collection('team_filter');

            let filters = {
                projects: [],
                statuses: [],
                sprints: [],
                teams: []
            };
            async.parallel([
                function(callback) {
                    projectsCollection.find({}).toArray(function(err, docs) {
                        filters.projects.push(docs);
                        callback()
                    });
                },
                function(callback) {
                    statusCollection.find({}).toArray(function(err, docs) {
                        filters.statuses.push(docs);
                        callback();
                    });
                },
                function(callback) {
                    sprintCollection.find({}).toArray(function(err, docs) {
                        filters.sprints.push(docs);
                        callback();
                    });
                },
                function(callback) {
                    teamCollection.find({}).toArray(function(err, docs) {
                        filters.teams.push(docs);
                        callback();
                    });
                },
            ], function(err, results) {
                res.send(filters);
            });
        });
    }
};