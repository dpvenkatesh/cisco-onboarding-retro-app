var path=require("path"),
    retroApi=require("../controllers/retroApi");
module.exports ={
    "get":[
        {
            "url":"/",
            "callback":function(req, res, next){
                res.send("Welcome to hxdhduger")
            }
        },
        {
            "url":"/get_all_retro_items",
            "callback":function(req, res, next){
                retroApi.getRetroItems('all',res);
            }
        },
        {
            "url":"/get_retro_item_by_id",
            "callback":function(req, res, next){
                retroApi.getFilterReport('_id',req.query.id,res);
            }
        },
        {
            "url":"/get_filters",
            "callback":function(req, res, next){
                retroApi.getFilters(res);
            }
        }
    ],
    "post":[
        {
            "url":"/save_retro_item",
            "callback":function(req, res, next){
                retroApi.saveRetroItems(req.body.item,res);
            }
        }
        ],
    "put":[]
};


