var registerModel=require("../models/retroModel"),
	errCode=require("../conf/error_codes"),
	async  = require('async');
module.exports =
{
    getRetroItems:function (typ,res) {
        if(typ==='all'){
            registerModel.getAllReport(res);
        }
    },
    saveRetroItems:function (itm,res) {
        registerModel.saveRetroItems(itm,res);
    },
    getFilterReport:function (field,vl,res) {
        registerModel.getFilterReport(field,vl,res);
    },
    getFilters:function (res) {
        registerModel.getFilters(res);
    }
};