var config = {
	development : {
		url : 'http://ds037195.mlab.com:37195',
        MONGODB_URI:'mongodb://a4p:a4pa4p@ds037195.mlab.com:37195/cisco',
		cicoDatabase : {
			host : 'localhost',
			user : 'a4p',
			password : 'a4pa4p',
			database : 'cisco',
			connectionLimit : 100, //important
			debug    :  false
		},
		//server details
		server : {
			host : '127.0.0.1',
			port : '37195'
		}
	},
	qa : {
		//url to be used in link generation
		url : 'http://ds037195.mlab.com:37195',
        MONGODB_URI:'mongodb://a4p:a4pa4p@ds037195.mlab.com:37195/cisco',
		cicoDatabase : {
			host : 'localhost',
			user : 'a4p',
			password : 'a4pa4p',
			database : 'cisco',
			connectionLimit : 100, //important
			debug    :  false
		},
		//server details
		server : {
			host : '127.0.0.1',
			port : '37195'
		}
	},
	production : {
		url : 'http://ds037195.mlab.com:37195',
        MONGODB_URI:'mongodb://a4p:a4pa4p@ds037195.mlab.com:37195/cisco',
		cicoDatabase : {
			host : 'localhost',
			user : 'a4p',
			password : 'a4pa4p',
			database : 'cisco',
			connectionLimit : 100, //important
			debug    :  false
		},
		//server details
		server : {
			host : '127.0.0.1',
			port : '37195'
		}
	}
};

module.exports = config;
